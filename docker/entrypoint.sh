#!/bin/sh
echo "*** Initializing container"

NODEV=""
if [ "$APPLICATION_ENV" = "production" ]; then
    NODEV="--no-dev"
fi

cd /app && composer install -n $NODEV

if [ ! -d "/app/runtime" ]; then
  echo "Creating runtime directory"
  mkdir "/app/runtime"
  chown www-data:www-data "/app/runtime"
  chmod 0774 "/app/runtime"
fi

# vendor/simplesamlphp/simplesamlphp/locales/hu/LC_MESSAGES/messages.po
# Replace 'metadataA"' to 'metadata"'
sed -i 's/metadataA"/metaadat"/' /app/vendor/simplesamlphp/simplesamlphp/locales/hu/LC_MESSAGES/messages.po

echo "*** Starting apache"

exec apache2-foreground
