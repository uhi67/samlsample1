Példa SAML bejelentkezés php alkalmazásba
=========================================

**v2.0**

Ez egy minimális példa, mely a lehető legkevesebb kóddal megmutatja, hogyan lehet php alkalmazást írni SAML bejelentkezéssel.
Ez eljárás bármilyen keretrendszer környezetében is alkalmazható, bár a keretrendszerek jellemzően saját eszközöket is kínálnak erre.
A példa a SimpleSAMLphp-t használja, de annak nem szükséges előre telepítve lennie, az alkalmazásba composer modulként települ.
A példa tartalmaz egy beépített teszt-idp-t is.

Minimális követelmények
-----------------------

- php 8.1
- composer

Telepítés
---------

- git clone
- composer install

Konfigurálás
-------------

### Apache konfig

Minta: `httpd-vhosts-template.conf`

### IIS konfig

Az apache konfig mintájára az IIS-ben is be kell állítani a megfelelő
- gyökér könyvtárat
- simplesaml virtuális directoryt (alias)
- a mappák jogait (runtime mappára write jog)

### Futtatás Docker alatt

1. Hozd létre a .env fájlt és töltsd ki a .env-template mintájára
2. Hozd létre a docker-compose.yml fájlt és töltsd ki a docker-compose-template.yml mintájára
3. docker compose up --build -d

### Környezeti változók

| változónév               | leírás                                                                                                       | default érték vagy példa                   |
|--------------------------|--------------------------------------------------------------------------------------------------------------|--------------------------------------------|
| SIMPLESAMLPHP_CONFIG_DIR | A 'simplesaml/config' mappának az abszolút címe a fájlrendszerben                                            | *** kötelező beállítani ***                |
| APPLICATION_ENV          | éles vagy fejlesztői környezet                                                                               | def. 'development', élesesben 'production' |
| HTTPS                    | Ha a reverse proxy terminálja a https-t, és nem érzékeli a HTTP_X_FORWARDED_PROTO alapján, akkor legyen 'on' | def üres vagy 'on'                         |
| SAML_APPNAME             | Alkalmazás neve, főleg a certfájl neve miatt érdekes                                                         | 'samlsample1'                              |
| SAML_IDP                 | Közvetlen IdP entitásazonosítója (ha nem az user választ)                                                    | null                                       | 
| SAML_DISCO               | Discovery szerviz URL címe (default a beépített)                                                             | null                                       |
| SAML_EDUID_CERT          | Ha EduID metaadatra is szükség van, akkor az EduID metaadatszerver tanúsítványa (fingerprint)                | null                                       |
| SAML_AUTH_SOURCE         | authsource bejegyzés neve, gyakran azonos az alkalmazásnévvel. Az entitásazonosítót befolyásolja             | 'default-sp'                               |

Használata
==========

Alapbeállításokkal az alkalmazás belépéskor közvetlenül a beépített teszt IdP-hez fordul. 

Amennyiben az alkalmazást regisztráltuk a PTE IdP-ben vagy az EduID-ben, és PTE-s vagy EduID bejelentkezést szeretnénk,
a `SAML_IDP` környezeti változóban adjuk meg a PTE IdP entitásazopnosítóját ('https://idp.pte.hu/saml2/idp/metadata.php'),
vagy a `SAML_DISCO` környezeti változóban adjuk meg az EduID discovery szerviz címét (https://discovery.eduid.hu)
Az EduID metaadatok használatához be kell állítani a `SAML_EDUID_CERT` értékét is (jelenleg '91:81:AD:2B:F1:C1:4E:47:93:A2:9D:49:34:B7:77:62:4F:2F:98:43')

## Beépített teszt IdP használata másik alkalmazáshoz

Ha fejlesztői környezetben teszt IdP-ként szeretnénk használni, akkor két regisztrációs feladat van:

1. Az alkalmazásunk (SP) regisztrálása ebben az IdP-ben
2. Ennek a teszt IdP-nek a regisztrálása az SP-ben

Példáinkban feltételezzük, hogy ez a szolgáltatás a `http://samlsample1.test` URL-en érhető el.

### SP regisztrálása az IdP-ben

A `simplesaml/metadata/private/saml20-sp-remote` fájlban (a template alapján hozzuk létre, ha nincs) vegyük fel az SP metaadatait.

### IdP regisztrálása az SP-ben

Az IdP metaadatai a `http://samlsample1.test/simplesaml/saml2/idp/metadata.php?output=xhtml` címen találhatók. 

Change log
==========
### v2.0

- Using SimpleSAMLphp >= 2.0
- php 8.1
- docker
- baseurl support

### v1.1

- internal test IdP function added
- using SimpleSAMLphp 1.18
- php 7.4

### v1.0

- minimal version, SP only
- using SimpleSAMLphp 1.16
- php 7.2

Tesztelés
----------

### Távoli teszt IdP használata

Ha a fejlesztés/tesztelés során a hivatalos helyett teszt-idp-t használunk, akkor a következő konfigurásiós módosítások szükségesek:

#### 1. A `simplesaml/config/config.php` fájlban
A 'metadata.sources' kulcs alatt elsőként vegyük fel a `['type' => 'flatfile'],` bejegyzést.

#### 2. A `simplesaml/metadata` mappában

Létre kell hozni a `saml20-idp-remote.php' fájlt, és abban bejegyezni manuálisan a teszt idp-t, például:

```
$metadata['http://testidp.pte.hu/saml2/idp/metadata.php'] = [
	'name' => [
		'hu' => 'Pécsi Tudományegyetem, Teszt',
		'en' => 'University of Pécs, Hungary, Test'
	],
	'description'   => 'Here you can login with a test account on PTE test IdP.',
	'certificate'   => '', // A kulcsot kérje az üzemeltetőktől, vagy a `http://testidp.pte.hu/saml2/idp/metadata.php` url-en  
	'SingleSignOnService'  => 'http://testidp.pte.hu/saml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'http://testidp.pte.hu/saml/saml2/idp/SingleLogoutService.php',
	'NameIDFormats' => [
		0 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
		1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
	],
];
```

A távoli idp tényleges metaadatai általában elérhetők az entityID url-jén.

#### 3. A `simplesaml/config/authsources.php` megfelelő bejegyzésében
Fel kell venni, hogy ezt az idp-t használjuk bejelentkezéshez, például:

	`'idp' => 'https://testidp.pte.hu/saml2/idp/metadata.php'`
	
A távoli teszt idp-n tesztfelhazsnálói adatokat annak üzemeltetőjénél lehet igényelni.

### Saját, beépített teszt IdP használata

A tesztidp-t meg lehet valósítani jelen simplesamlphp telepítéssel is. Ennek beállítása:
(feltételezzük, hogy a tesztalkalmazás url-je `http://app.local`)

#### 1. A `simplesaml/config/congig.php` fájlban

Be kell kapcsolni a `'enable.saml20-idp' => true,` beállítást (gyárilag false)

#### 2. A `simplesaml/config/authsources.php`	fájlban 

Elő van készítve a `localtest` bejegyzés, itt lehet felvenni tesztfelhasználókat az attributumértékeivel együtt.

#### 3. A `simplesml/cert` mappában

Létre kell hozni a `local.pem`/`local.crt` kulcspárt (lásd simplesamlphp dokumentáció)

#### 4. A `simplesaml/metadata` mappában

Létre kell hozni a `saml20-idp-hosted.php` fájlt, és bejegyezni a saját idp metaadatait, például:

```
$metadata['http://app.local/simplesaml/saml2/idp/metadata.php'] = [
	'host' => '__DEFAULT__',
	'auth' => 'localtest',
	'name' => 'Helyi teszt bejelentkezés',
	'privatekey' => 'local.pem',
	'certificate' => 'local.crt',
	'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
	'AttributeNameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
	'userid.attribute' => 'uid', // (uid) Itt adjuk meg, hogy mely, az LDAPból származó attribútum alapján fogja az IdP kiszámítani az eduPersonTargetedID-t
	'authproc' => [
		// Állandó attributumok hozzáadása, például
		21 => [
		   //Statikus attribútum, az intézmény domain azonosítója
		   'class' => 'core:AttributeAdd',
		   'schacHomeOrganization' => ['app.local']
		],
	],
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
	'simplesaml.nameidattribute' => 'urn:oid:1.3.6.1.4.1.5923.1.1.1.6', //'eduPersonPrincipalName',
	'NameIDFormats' => [
		'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
		'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
	],
	'attributeencodings' => [
		'urn:oid:1.3.6.1.4.1.5923.1.1.1.10' => 'raw',
		'eduPersonTargetedID' => 'raw',
	],
	'redirect.sign' => true,
];
```

#### 5. A `simplesaml/config/config.php` fájlban 

A 'metadata.sources' kulcs alatt elsőként vegyük fel a `['type' => 'flatfile'],` bejegyzést.

#### 6. A `simplesaml/metadata` mappában

Létre kell hozni a `saml20-idp-remote.php` fájlt, és abban bejegyezni manuálisan a teszt idp-t:

```
$metadata['http://app.local/simplesaml/saml2/idp/metadata.php'] = [
   'name' => [
	   'hu' => 'Helyi teszt bejelentkezés',
	   'en' => 'Local test authentication source'
   ],
   'description'          => 'Here you can login with a test account.',
   'SingleSignOnService'  => 'http://app.local/simplesaml/saml2/idp/SSOService.php',
   'SingleLogoutService'  => 'http://app.local/simplesaml/saml2/idp/SingleLogoutService.php',
   'certificate' => 'local.crt',
];
```

#### 7. A `simplesaml/config/authsources.php` megfelelő bejegyzésében

Fel kell venni, hogy ezt az idp-t használjuk bejelentkezéshez, például:

```
   'idp' => 'https://app.local/simplesaml/saml2/idp/metadata.php'
```

Ebben az esetben tesztfelhasználókat saját magunknak tudunk beállítani, lásd a fenti 2. pontot.
