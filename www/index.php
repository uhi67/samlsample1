<?php

use SimpleSAML\Auth\Simple;
use SimpleSAML\Utils\HTTP;

$baseurl = require dirname(__DIR__).'/simplesaml/baseurl.php';

$env = getenv('APPLICATION_ENV');
if($env != "production" ) {
	ini_set('display_errors', 'stdout');
}

require_once dirname(__DIR__).'/vendor/autoload.php';

$as = new Simple('default-sp');

if(array_key_exists('logout', $_REQUEST)) {
    $httpUtils = new HTTP();
	$as->logout($httpUtils->getSelfURLNoQuery());
}
if(array_key_exists('login', $_REQUEST)) {
	$as->requireAuth([
        'ReturnTo' => $baseurl,
    ]);
}

echo '<h1>SimpleSAMLphp példa</h1>';
if($as->isAuthenticated()) {
	$attributes = $as->getAttributes();
	echo "<h2>Üdvözöljük, ". $attributes['displayName'][0]. '!</h2>';
	echo '<form><button name="logout">Kilépés</button></form>';
}
else {
	echo '<h2>Nincs belépve</h2>';
	echo '<form><button name="login">Belépés</button></form>';
}
