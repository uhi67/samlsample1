<?php

$idp = getenv('SAML_IDP') ?: null; // Default is the user-selected IdP
$disco = getenv('SAML_DISCO') ?: null; // Default is the built-in discovery service
$baseurl = require dirname(__DIR__).'/baseurl.php';

$config = [

    // This is a authentication source which handles admin authentication.
    'admin' => [
        // The default is to use core:AdminPassword, but it can be replaced with
        // any authentication source.

        'core:AdminPassword',
	],

    'localtest' => [
        'exampleauth:UserPass',
        'test0:test0' => [
            'uid' => ['test1'],
            'displayName' => 'test0',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test0@test.test',
            'mail' => 'test0@test.test',
            'niifPersonOrgID' => ['LOTE0AAA'],
        ],
        'test1:test1' => [
            'uid' => ['test1'],
            'displayName' => 'Teszt Elek',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test1@test.test',
            'mail' => 'test1@test.test',
            'niifPersonOrgID' => ['LOTE1AAA'],
        ],
        'test2:test2' => [
            'uid' => ['test2'],
            'displayName' => 'Teszt Kettes',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test2@test.test',
            'mail' => 'test2@test.test',
            'niifPersonOrgID' => ['LOTE2AAA'],
        ],
        'test3:test3' => [
            'uid' => ['test3'],
            'displayName' => 'Teszt Harmad',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test3@test.test',
            'mail' => 'test3@test.test',
            'niifPersonOrgID' => ['LOTE3AAA'],
        ],
    ],

    // An authentication source which can authenticate against both SAML 2.0
    // and Shibboleth 1.3 IdPs.
    'default-sp' => [
        'saml:SP',

        // The entity ID of this SP.
        // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
        'entityID' => $baseurl.'/sp',

        // The entity ID of the IdP this SP should contact.
        // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
        'idp' => $idp,

        // The URL to the discovery service.
        // Can be NULL/unset, in which case a builtin discovery service will be used.
        'discoURL' => $disco,

		'OrganizationName' => [
			'hu' => 'Pécsi Tudományegyetem',
			'en' => 'University of Pécs',
		],
		'OrganizationDisplayName' => [
		    'hu' => 'Pécsi Tudományegyetem.',
		    'en' => 'University of Pécs',
		],
		'OrganizationURL' => [
		    'hu' => 'http://www.pte.hu/',
		],
        /*
         * WARNING: SHA-1 is disallowed starting January the 1st, 2014.
         *
         * Uncomment the following option to start using SHA-256 for your signatures.
         * Currently, SimpleSAMLphp defaults to SHA-1, which has been deprecated since
         * 2011, and will be disallowed by NIST as of 2014. Please refer to the following
         * document for more information:
         *
         * http://csrc.nist.gov/publications/nistpubs/800-131A/sp800-131A.pdf
         *
         * If you are uncertain about identity providers supporting SHA-256 or other
         * algorithms of the SHA-2 family, you can configure it individually in the
         * IdP-remote metadata set for those that support it. Once you are certain that
         * all your configured IdPs support SHA-2, you can safely remove the configuration
         * options in the IdP-remote metadata set and uncomment the following option.
         *
         * Please refer to the hosted SP configuration reference for more information.
          */
        'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',

        /*
         * The attributes parameter must contain an array of desired attributes by the SP.
         * The attributes can be expressed as an array of names or as an associative array
         * in the form of 'friendlyName' => 'name'.
         * The metadata will then be created as follows:
         * <md:RequestedAttribute FriendlyName="friendlyName" Name="name" />
         */
        /*'attributes' => array(
            'attrname' => 'urn:oid:x.x.x.x',
        ),*/
        /*'attributes.required' => array (
            'urn:oid:x.x.x.x',
        ),*/
		'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
	],
];

$configDir = dirname(__DIR__, 2) .'/config';
if(file_exists($localFile = $configDir.'/local_users.php')) {
	$local = include $localFile;
	if(is_array($local)) {
		$config['pte-ws']['local'] = array_merge($config['pte-ws']['local'], $local);
	}
}

if(file_exists(dirname(__DIR__).'/local-users.php')) {
    /** @noinspection PhpIncludeInspection
     * @noinspection RedundantSuppression -- a fájl nem mindig van
     */
    $users = require dirname(__DIR__).'/local-users.php';
    $config['localtest'] = array_merge($config['localtest'], $users);
}
