<?php /** @noinspection DuplicatedCode */
/**
 * SAML 2.0 remote SP metadata for simpleSAMLphp.
 * See: http://simplesamlphp.org/docs/trunk/simplesamlphp-reference-sp-remote
 */

/** @noinspection PhpUnhandledExceptionInspection */

$baseurl = require dirname(__DIR__).'/baseurl.php';
$authSource = getenv('SAML_AUTH_SOURCE') ?: 'default-sp';
$privacyStatementURL = getenv('SAML_PRIVACY') ?: $baseurl;

// itself as SP
$metadata[$baseurl.'/sp'] = [
	'AssertionConsumerService' => $baseurl.'/simplesaml/module.php/saml/sp/saml2-acs.php/'.$authSource,
	'SingleLogoutService' => $baseurl.'/simplesaml/module.php/saml/sp/saml2-logout.php/'.$authSource,
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    'PrivacyStatementURL' => [ 'hu' => $privacyStatementURL ],
  	'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
];
