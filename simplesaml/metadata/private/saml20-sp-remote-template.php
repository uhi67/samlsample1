<?php
/**
 * SAML 2.0 remote SP metadata for simpleSAMLphp.
 * See: {@see http://simplesamlphp.org/docs/trunk/simplesamlphp-reference-sp-remote}
 */

/** Sample remote SP */
$metadata['--entitásazonosító--'] = array (
	'AssertionConsumerService' => '', // Kötelező kitölteni az SP metaadataiból.
    //  Az adat ebben a sorban a Location: <md:AssertionConsumerService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="http://myexamplesp.test/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp" index="0"/>

	'SingleLogoutService' => 'http://myexamplesp.test/simplesaml/module.php/saml/sp/saml2-logout.php/default-sp',    // Erősen ajánlott kitölteni
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient', // default érték, de szükség esetén módosítható
  	'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri', // default érték, de szükség esetén módosítható
);
