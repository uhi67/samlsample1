<?php
return [
    /* Minta teszt felhasználó. A kulcs a 'login:jelszó' páros */
    'test9:test9' => [
        'uid' => ['test9'],
        'displayName' => 'TesztEL9',
        'schacHomeOrganization' => 'pte.hu',
        'eduPersonAffiliation' => ['member', 'employee'],
        'eduPersonPrincipalName' => 'test9@test.test',
        'mail' => 'test9@test.test',
        'niifPersonOrgID' => ['LOTE9AAA'],
    ],
];
